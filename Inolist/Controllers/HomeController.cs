﻿using System.Web.Mvc;

namespace InoList.Controllers
{
    public class HomeController : Controller

    {
        [Authorize]
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Task");
            }

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}