﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Logic.VK.Entity;
using InoList.Website.Core;
using InoList.Models.DataBase;
using InoList.Models;
using Logic.VK.Repository;

namespace InoList.Controllers
{
    public class AccountController : AbstractController
    {
        private readonly DataContext _db = new DataContext();

        public ActionResult NotLogger(String ReturnUrl, Int32? senderId)
        {
            if (ReturnUrl != null)
            {
                if (senderId != null)
                {
                    TempData["Redirect"] = ReturnUrl + "&&" + senderId;
                }
                else 
                {
                    TempData["Redirect"] = ReturnUrl;
                }
            }
            return View();
        }

        public ActionResult GetVkAuth(String email)
        {
            if (email != null &&
                System.Text.RegularExpressions.Regex.IsMatch(email,
                                                             @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$"))
            {
                TempData["email"] = email;
            }
            return (new Logic.VK.Auth()).Authorization();
        }

        public ActionResult LogOn(String code)
        {
            var auth = new Logic.VK.Auth();
            VKSecurity model = auth.GetAccessToken(code);

            if (model != null)
            {
                Int32 convertId = Convert.ToInt32(model.user_id);
                var user = _db.Users.FirstOrDefault(x => x.VkontakteId == convertId);
                if (user != null)
                {
                    Session.DoLogin(user, true);
                    return Redirect("http://denakol.ru" + (String) TempData["Redirect"]);
                }
                TempData["model"] = model;
                return Redirect(Url.Action("Register"));
            }
            return View("Error", new Error {ErrorText = "Проблема с авторизацией в контакте"});
        }

        public ActionResult LogOff()
        {
            Session.Logout();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            var email = (String) TempData["email"];
            if (email == null)
            {
                return View();
            }
            var user = RegisterUser(email);
            Session.DoLogin(user, false);
            ConfirmInvites(email);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            var user = RegisterUser(model.Email);
            Session.DoLogin(user, false);
            return RedirectToAction("Index", "Home");
        }

        private void ConfirmInvites(String email)
        {
            Invites invites = _db.Invites.First(x => x.ResipientEmail == email);
            invites.ConfirmDate = DateTime.Now;
            invites.RecipientId = _db.Users.First(x => x.Email == email).Id;
            _db.Entry(invites).State = System.Data.EntityState.Modified;
            _db.SaveChanges();
        }

        private UserData RegisterUser(String email)
        {
            var userVk = (VKSecurity) TempData["model"];
            var data = (new VKUserRepo()).GetUsers(((String) userVk.user_id),
                                                   "uid, first_name, last_name, nickname, screen_name,photo", "20",
                                                   "nom", ((String) userVk.access_token));
            var vkuser = data.response[0];

            var user = new UserData
                {
                    FirstName = vkuser.first_name,
                    LastName = vkuser.last_name,
                    Email = email,
                    VkontakteId = Convert.ToInt32(vkuser.uid),
                    AvatarPath = vkuser.photo,
                    VkAccessToken = userVk.access_token
                };
            if (_db.Users.FirstOrDefault(x => x.VkontakteId == user.VkontakteId) == null)
            {
                _db.Users.Add(user);
                _db.SaveChanges();
            }
            return user;
        }

    }
}
