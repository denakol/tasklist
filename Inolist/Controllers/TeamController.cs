﻿using System;
using System.Linq;
using System.Web.Mvc;
using InoList.Models.DataBase;
using InoList.Website.Core;
using InoList.Models.View;
using System.Data.Entity;
using InoList.Models;
using MailLibrary;

namespace InoList.Controllers
{
    [Authorize]
    public class TeamController : AbstractController
    {
        private readonly DataContext _db = new DataContext();

        public ActionResult Index(String filter)
        {
            var users =
                _db.Invites.Where(x => (x.SenderId == Session.User.Id || x.RecipientId == Session.User.Id) && (x.ExcludeId == null || Session.User.Id != x.ExcludeId))
                   .Include(x => x.Recipient)
                   .Include(x => x.Sender)
                   .AsEnumerable();
            var viewUser = users.Select(x => new ViewUser(x, Session.User.Id));

            bool filterEnable = false;
            bool filterTeam = false;
            switch (filter)
            {
                case "team":
                    filterEnable = true;
                    filterTeam = true;
                    break;
                case "notTeam":
                    filterEnable = true;
                    break;
            }
            if (filterEnable)
            {
                viewUser = viewUser.Where(x => x.Team == filterTeam);
            }
            return View(viewUser.ToList());
        }


        public ActionResult Add()
        {
            var model = new RegisterModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                String error = String.Empty;
                if (model.Email != Session.UserEmail)
                {
                    var team = _db.Invites.Where(x => x.SenderId == Session.User.Id || x.RecipientId == Session.User.Id);
                    bool isTeam = false;
                    bool isReSend = false;
                    if (team.Any())
                    {
                        isTeam =
                            team.Any(
                                x =>
                                (x.Recipient.Email == model.Email || x.Sender.Email == model.Email) &&
                                (x.ConfirmDate != null));
                        isReSend = team.Any(x => x.ResipientEmail == model.Email && x.RecipientId == null);
                    }

                    if (!isTeam)
                    {
                        if (!isReSend)
                        {
                            _db.Invites.Add(new Invites
                                {
                                    CreatedDate = DateTime.Now,
                                    ResipientEmail = model.Email,
                                    SenderId = Session.User.Id,
                                });
                            _db.SaveChanges();
                        }
                        if (!_db.Users.Any(x => x.Email == model.Email))
                        {
                            Mail.SendInvitesNewUser(model.Email, Session.User.Email,
                                                    Session.User.LastName + " " + Session.User.FirstName);
                        }
                        else
                        {
                            Mail.SendInvitesOldUser(model.Email, Session.User.Email,
                                                    Session.User.LastName + " " + Session.User.FirstName,
                                                    Session.User.Id);
                        }
                        return RedirectToAction("Index");
                    }
                    error = "Пользователь уже в команде";
                }
                else
                {
                    error = "Введите не свой email";
                }
                var modelReturn = new RegisterModel {Error = error};
                return View(modelReturn);
            }
            return View(new RegisterModel {Error = null});
        }


        public ActionResult Confirm(String email, Int32 senderId)
        {
            if (senderId != Session.User.Id && email==Session.UserEmail)
            {
                Invites invite = _db.Invites.First(x => x.ResipientEmail == email && x.SenderId == senderId);
                invite.ConfirmDate = DateTime.Now;
                invite.RecipientId = _db.Users.First(x => x.Email == email).Id;
                invite.ExcludeId = null;
                _db.Entry(invite).State = System.Data.EntityState.Modified;
                _db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPut]
        public ActionResult Exclude(Int32 id)
        {
            var tasks = _db.Tasks.Where(x => x.ExecutorId == id && x.AuthorID == Session.User.Id && (!x.IsArchival));
            foreach (var task in tasks)
            {
                task.ExecutorId = null;
            }
            var invite = _db.Invites.First(x => x.RecipientId == id && x.SenderId == Session.User.Id
                                       || x.RecipientId == Session.User.Id && x.SenderId == id);
            invite.ConfirmDate = null;
            invite.CreatedDate = null;
            if (invite.SenderId != Session.User.Id)
            {
                invite.ResipientEmail= invite.Sender.Email;
                invite.RecipientId = invite.SenderId;
                invite.SenderId = Session.User.Id;
            }
            invite.ExcludeId = id;
            _db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }

        [HttpPut]
        public ActionResult ReSend(String email)
        {
            var user = _db.Users.FirstOrDefault(x=>x.Email==email);
            if (user == null)
            {
                Mail.SendInvitesNewUser(email, Session.User.Email, Session.User.LastName +" "+ Session.User.FirstName);
            }
            else
            {
                Mail.SendInvitesOldUser(user.Email, Session.User.Email, Session.User.LastName +" "+ Session.User.FirstName, Session.User.Id);
            }

            var invite = _db.Invites.First(x=> x.SenderId == Session.User.Id || x.RecipientId == Session.User.Id);
            invite.CreatedDate = DateTime.Now;
            _db.SaveChanges();
            return new HttpStatusCodeResult(200);
        }


        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }
}
