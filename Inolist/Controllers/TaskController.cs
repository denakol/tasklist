﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InoList.Models.DataBase;
using InoList.Website.Core;
using InoList.Models.View;
using MailLibrary;

namespace InoList.Controllers
{
    [Authorize]
    public class TaskController : AbstractController
    {
        private readonly DataContext _db = new DataContext();

        //
        // GET: /Task/

        public ViewResult Index(String filter)
        {
            bool filterInput = false;
            bool filterOutput = false;
            switch (filter)
            {
                case "input":
                    filterOutput = true;
                    break;
                case "output":
                    filterInput = true;
                    break;
            }

            var userId = Session.User.Id;
            var tasks = _db.Tasks.Where(x => !x.IsArchival)
                           .Where(x => ((!filterOutput) && (x.AuthorID == userId ))
                                       || ((!filterInput) && x.ExecutorId == userId && x.AuthorID!= userId))
                           .Include(x => x.Author)
                           .Include(x => x.Executor)
                           .AsEnumerable();
            var tasksView = tasks.Select(x => new ViewTask(x)).OrderBy(x => x.EndDate, ViewTask.SortEndDate());


            return View(tasksView.ToList());
        }


        [HttpGet]
        public ActionResult Add()
        {
            AddSelectListExecutor(null);
            return PartialView("PopupAdd");
        }   

        [HttpPost]
        public ActionResult Add(TaskData taskdata)
        {
            if (ModelState.IsValid)
            {
                taskdata.CreateDate = DateTime.Now;
                taskdata.AuthorID = Session.User.Id;
                taskdata.Title = taskdata.Title.Trim();
                taskdata.Description = taskdata.Description.Trim();
                _db.Tasks.Add(taskdata);
                _db.SaveChanges();
                if (taskdata.ExecutorId != null && taskdata.ExecutorId != taskdata.AuthorID && taskdata.IsAlert)
                {
                    Mail.SetTaskExecutor(_db.Users.FirstOrDefault(x => x.Id == taskdata.ExecutorId).Email,
                                         Session.UserEmail,
                                         Session.User.FirstName + Session.User.LastName, taskdata.Title,
                                         taskdata.EndDate);
                }
                return RedirectToAction("Index");
            }
            AddSelectListExecutor(taskdata.ExecutorId);
            return PartialView("PopupAdd");
        }


        [HttpGet]
        public ActionResult Edit(Int32 id)
        {
            TaskData taskdata = _db.Tasks.Find(id);
            if (taskdata.AuthorID == Session.User.Id)
            {
                AddSelectListExecutor(taskdata.ExecutorId);
                return PartialView("PopupEditAuthor", taskdata);
            }
            return PartialView("PopupEditExecutor", taskdata);
        }

        [HttpPost]
        public ActionResult Edit(TaskData taskNew)
        {
            var taskPast = _db.Tasks.Find(taskNew.Id);
                if (taskPast.AuthorID == Session.User.Id)
                {
                    taskPast.Title = taskNew.Title.Trim();
                    taskPast.Description = taskNew.Description.Trim();
                    taskPast.EndDate = taskNew.EndDate;
                    taskPast.IsAlert = taskNew.IsAlert;
                    if (taskPast.IsAlert)
                    {
                        UserData executorNew = null;
                        UserData executorPast = null;
                        if (Session.User.Id != taskNew.ExecutorId && taskNew.ExecutorId != null)
                        {
                            executorNew = _db.Users.FirstOrDefault(x => x.Id == taskNew.ExecutorId);
                        }
                        if (Session.User.Id != taskPast.ExecutorId && taskPast.ExecutorId != null)
                        {
                            executorPast = _db.Users.FirstOrDefault(x => x.Id == taskPast.ExecutorId);
                        }
                        if (executorNew != executorPast)
                        {
                            if (executorNew != null)
                            {
                                Mail.SetTaskExecutor(executorNew.Email,
                                                     Session.UserEmail,
                                                     Session.User.FirstName +" " + Session.User.LastName, taskNew.Title,
                                                     taskNew.EndDate);
                            }
                            if (executorPast != null)
                            {
                                Mail.SetOffTaskExecutor(executorPast.Email,
                                                        Session.UserEmail,
                                                        Session.User.FirstName +" "+ Session.User.LastName, taskNew.Title,
                                                        taskNew.EndDate);
                            }
                        }
                        else
                        {
                            if (executorNew != null)
                            {
                                Mail.TaskEdit(executorNew.Email,
                                              Session.UserEmail,
                                              Session.User.FirstName + " "+Session.User.LastName, taskNew.Title);
                            }
                        }
                    }

                    taskPast.ExecutorId = taskNew.ExecutorId;
                }
                else
                {
                    taskPast.Description = taskNew.Description.Trim();
                    taskPast.EndDate = taskNew.EndDate;
                    Mail.TaskEdit(taskPast.Author.Email,
                                              Session.UserEmail,
                                              Session.User.FirstName +" " +Session.User.LastName, taskNew.Title);
 

                }
                _db.SaveChanges();
                return RedirectToAction("Index");
            
        }


        [HttpDelete]
        public ActionResult Delete(Int32 id)
        {
            var task = _db.Tasks.FirstOrDefault(x => x.Id == id);
            if (task.AuthorID == Session.User.Id)
            {
                if (task.ExecutorId != null && task.ExecutorId != task.AuthorID && task.IsAlert)
                {
                    Mail.TaskDelete(task.Executor.Email, Session.UserEmail,
                                    Session.User.FirstName + " " + Session.User.LastName, task.Title);
                }
                _db.Tasks.Remove(task);
                _db.SaveChanges();
            }

            return new HttpStatusCodeResult(202);
        }


        [HttpPut]
        public HttpResponse Done(Int32 id)
        {
            var task = _db.Tasks.Find(id);
            if (task.ExecutorId != null && task.ExecutorId != task.AuthorID && task.IsAlert)
            {
                String email = Session.User.Id != task.ExecutorId ? task.Executor.Email : task.Author.Email;
                if (!task.IsDone)
                {
                    Mail.TaskDone(email, Session.UserEmail,
                                  Session.User.FirstName + " " + Session.User.LastName, task.Title);
                }
                else
                {
                    Mail.TaskReOpen(email, Session.UserEmail,
                                    Session.User.FirstName + " " + Session.User.LastName, task.Title);
                }
            }
            task.IsDone = !task.IsDone;
            _db.Entry(task).State = EntityState.Modified;
            _db.SaveChanges();
            return null;
        }

        [HttpPut]
        public HttpResponse Archive()
        {
            var tasks = _db.Tasks.Where(x => x.AuthorID == Session.User.Id && x.IsDone);
            if (tasks.Any())
            {
                foreach (var taskData in tasks)
                {
                    taskData.IsArchival = true;
                }
                _db.SaveChanges();
            }

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }


             private void AddSelectListExecutor(Int32? id)
        {
            var team = _db.GetUserSelectList(Session.User.Id);
            ViewBag.ExecutorId =
                new SelectList(team.Select(x => new
                {    FullName =  x.FirstName + " " + x.LastName ,          
                   
                   Id = x.Id
                }), "Id", "FullName",id);
        }    
    }
}