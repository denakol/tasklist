﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;

using InoList.Auth;
using InoList.Models.DataBase;

namespace InoList.Website.Core
{
    public class UserSession : AbstractUserSession
    {
        #region public properties

        /// <summary>
        /// Session user
        /// </summary>
        public new UserData User
        {
            get
            {
                return (UserData)base.User;
            }
        }
        
        #endregion public properties

        #region protected methods

        /// <summary>
        /// Method is run after login
        /// </summary>
        protected override void OnLogin(IPrincipal user)
        {
            base.OnLogin(user);
        }

        /// <summary>
        /// Method is run after logout
        /// </summary>
        protected override void OnLogout()
        {
        }

        /// <summary>
        /// Get authenticated user
        /// </summary>
        protected override IPrincipal GetUser(String email)
        {
            return (new DataContext()).Users.FirstOrDefault(x=> x.Email == email);
        }

        #endregion protected methods
    }
}