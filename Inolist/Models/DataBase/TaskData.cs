﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InoList.Models.DataBase
{
    public class TaskData
    {
        public Int32 Id { get; set; }

        [Display(Name = "Задача")]
        [Required]
        [StringLength(500)]
        public String Title { get; set; }

        [Display(Name = "Описание")]
        [Required]
        [StringLength(10000)]
        public String Description { get; set; }

        [ForeignKey("Author")]
        public Int32 AuthorID { get; set; }

        public virtual UserData Author { get; set; }


        public DateTime CreateDate { get; set; }


        [Display(Name = "Дата исполнения")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime? EndDate { get; set; }

        [ForeignKey("Executor")]
        [Display(Name = "Исполнитель")]
        public Int32? ExecutorId { get; set; }

        public virtual UserData Executor { get; set; }
        public Boolean IsDone { get; set; }
        public Boolean IsAlert { get; set; }
        public Boolean IsArchival { get; set; }
    }
}