﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace InoList.Models.DataBase
{
    public class DataContext: DbContext
    {
        public DbSet<TaskData> Tasks { get; set; }
        public DbSet<UserData> Users { get; set; }
        public DbSet<Invites> Invites { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); 
        }

        public IQueryable<UserData> GetUserSelectList(int id)
        {
            var invite = Invites.Where(x => (x.ConfirmDate != null) && (x.SenderId == id || x.RecipientId == id));
            var left = invite.Join(Users,outer=> outer.RecipientId,inner=>inner.Id, (outer,inner)=> inner).Distinct();
            var right = invite.Join(Users, outer => outer.SenderId, inner => inner.Id, (outer, inner) => inner).Distinct();
            var team = left.Concat(right).Distinct();
            if (!team.Any())
            {
                return Users.Where(x => x.Id == id);
            }
            return team;
        }
        
    }
}