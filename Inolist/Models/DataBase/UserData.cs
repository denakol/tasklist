﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using InoList.Auth;



namespace InoList.Models.DataBase
{
    public class UserData : MyAbstractUser , IPrincipal
    {
        public Int32 Id { get; set; }

        public Int64 VkontakteId { get; set; }
        public String VkAccessToken { get; set; }

        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String AvatarPath { get; set; }

        [InverseProperty("Author")]
        public virtual ICollection<TaskData> InboxTasks { get; set; }

        [InverseProperty("Executor")]
        public virtual ICollection<TaskData> OutboxTasks { get; set; }

        [InverseProperty("Sender")]
        public virtual ICollection<Invites> SentInvites { get; set; }

        [InverseProperty("Recipient")]
        public virtual ICollection<Invites> ReceivedInvites { get; set; }
        public virtual IIdentity Identity
        {
            get
            {
                if (_identity == null)
                {
                    _identity = new GenericIdentity(VkontakteId.ToString());
                }

                return _identity;
            }
        }






        public virtual Boolean IsInRole(String role)
        {
            return false;
        }


        private IIdentity _identity;

        
    }
}