﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace InoList.Models.DataBase
{
    public class Invites
    {
        public Int32 Id { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ConfirmDate { get; set; }

        public String ResipientEmail { get; set; }

        [ForeignKey("Sender")]
        public Int32 SenderId { get; set; }

        public virtual UserData Sender { get; set; }

        [ForeignKey("Recipient")]
        public Int32? RecipientId { get; set; }

        public virtual UserData Recipient { get; set; }

        public Int32? ExcludeId { get; set; }
    }
}