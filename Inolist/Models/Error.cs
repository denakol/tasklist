﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace InoList.Models
{
    public class Error
    {
        public String ErrorText { get; set; }
    }
}