﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using InoList.Models.DataBase;
using System.Collections;



namespace InoList.Models.View
{
    public class ViewTask
    {
        public ViewTask(TaskData x)
        {
            Id = x.Id;
            Title = x.Title;
            Description = x.Description;
            AuthorID = x.AuthorID;
            CreateDate = x.CreateDate;
            EndDate = x.EndDate;
            AuthorInicial = String.Format("{0}.{1}.", x.Author.FirstName[0], x.Author.LastName[0]);
            AuthorAvatarPath = x.Author.AvatarPath;
            AuthorFullName = String.Format("{0} {1}", x.Author.FirstName, x.Author.LastName);
       
            IsDone = x.IsDone;
            IsAlert = x.IsAlert;
            if (x.ExecutorId != null)
            {
                ExecutorInicial = String.Format("{0}.{1}.", x.Executor.FirstName[0], x.Executor.LastName[0]);
                ExecutorAvatarPath = x.Executor.AvatarPath;
                ExecutorFullName = String.Format("{0} {1}", x.Executor.FirstName, x.Executor.LastName);
            }
                IsOverdue = EndDate < DateTime.Now;

        }



        
        public Int32 Id { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public Int32 AuthorID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EndDate { get; set; }

        public String AuthorInicial { get; set; }
        public String AuthorFullName { get; set; }
        public String AuthorAvatarPath { get; set; }

        public String ExecutorInicial { get; set; }
        public String ExecutorFullName { get; set; }
        public String ExecutorAvatarPath { get; set; }

        public Boolean IsDone { get; set; }
        public Boolean IsAlert { get; set; }
        public Boolean IsOverdue { get; set; }
        public static IComparer<DateTime?> SortEndDate()
        {
            return (IComparer<DateTime?>)new SortDate();
        }

         private class SortDate : IComparer<DateTime?>
        {
             public int Compare(DateTime? a, DateTime? b)
            {   
                if (a> b)
                {
                        return 1;
                }
                if (a < b)
                {
                    return -1;
                }
                if (a == null && b == null)
                {
                    return 0;
                }
                if (a == null)
                {
                    return 1;
                }
                if (b == null)
                {
                    return -1;
                }
                 return 0;
            }
        }



   }
}

