﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;
using InoList.Auth;
using InoList.Models.DataBase;



namespace InoList.Models.View
{
    public class ViewUser
    {

        public ViewUser(Invites invites, Int32 userId)
        {
            if (invites.Recipient != null)
            {
                if (invites.Recipient.Id == userId)
                {
                    invites.RecipientId = invites.SenderId;
                    invites.Recipient = invites.Sender;
                    isSender = true;
                }
                else
                {
                    isSender = false;
                }
            }
              Id = invites.RecipientId ?? -1;
              FullName = invites.Recipient != null ? String.Format("{0} {1}", invites.Recipient.FirstName, invites.Recipient.LastName) : invites.ResipientEmail;
              AvatarPath = invites.Recipient != null ? invites.Recipient.AvatarPath : "~/Content/Images/user.png";
              Invites = invites.CreatedDate;
              Confirm = invites.ConfirmDate;
              Team = invites.ConfirmDate != null;
              Exclude = Confirm == null;
              RecipientEmail = invites.ResipientEmail;

            
        }


        public Int32? Id { get; set; }
        public String FullName { get; set; }
        public String RecipientEmail { get; set; }
        public String AvatarPath { get; set; }
        public DateTime? Invites { get; set; }
        public DateTime? Confirm { get; set; }
        public bool Team { get; set; }
        public bool Exclude { get; set; }

        public bool isSender{ get;set;}



    }
}