﻿var baseUri='http://denakol.ru/';

$(document).ready(function () {
    var url = document.location.href;

    $("#navbar li ").find("a").each(function () {
        if (this.href == url) { $(this).parent().addClass('active'); };
    });
    $(document).click(function (event) {
        if ($(event.target).closest(".alert").length) return;
        if ($(event.target).closest("#archive").length) return;
        $(".alert").hide("slow");
        event.stopPropagation();
    });



    $('.divEditTask').click(function () {
        $('#task-popup').empty();
        var id = $(this).parent().find('#id').val();
        $.ajax({
            type: "GET",
            url: baseUri + 'Task/Edit/' + id,
            success: function (data) {
                $('#task-popup').empty();
                $('#task-popup').append(data);
            }
        });
        $('#task-popup').modal();
    });
    $('#addTask').click(function () {
        $.ajax({
            type: "GET",
            url: baseUri + 'Task/Add/',
            success: function (data) {
                $('#task-popup').empty();
                $('#task-popup').append(data);
            }
        });
        $('#task-popup').modal();
        return false;
    });

    $('.tasks').each(function () {
        var bool = ($(this).find("#IsOverdue").val()) == "True";
        if (bool) {
            $(this).addClass('alert-error');
        }
        if ($(this).find('input[type=checkbox]').attr('checked') == 'checked') {
            $(this).addClass('done');
            $(this).removeClass('alert-error');
        }


    });
    $('.tasks #IsComplete ').change(function () {
       
        var id = $(this).parent().parent().find('#id').val();
        var data = { Id: id };
        $.ajax({
            type: "Put",
            url: baseUri + 'Task' + '/Done',
            dataType: 'json',
            data: data
        });
        $(this).parent().parent().toggleClass('done');
    });
    $('#archive').click(function () {
        $('.alert').show();
    });
    $('.closeAlert').click(function () {
        $('.alert').hide('fast');
    });

    $('#archiveConfirm').click(function () {
        $.ajax({
            type: "Put",
            url: baseUri + 'Task' + '/Archive',
            statusCode: {
                200: function (data) {
                    location.reload();
                }
            }
        });

    });

    $('.team #isTeam').change(function () {
        
        var pop = $(this).popover({
            trigger: 'manual',
            content: "У исполнителя могут быть задачи. Вы уверены?",
            template: '<div class= "popover"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div><button class="btn btn-danger" id="popover-yes">Да</button><button class="btn btn-primary pull-right" id="popover-no">Нет</button></div>'
        });
        $(this).popover('show');
        $(this).prop("checked", true);


    });
    $("body").on("click","#popover-yes", function () {

        var id = $(this).parent().parent().parent().parent().find('#id').val();
        var data = { Id: id };
        var textBox = $(this);
        $.ajax({
            type: "Put",
            url: baseUri + 'Team' + '/Exclude',
            dataType: 'json',
            data: data,
            statusCode: {
                200: function(data) {
                    location.reload();
                }
            }
        });
    });
    $("body").on("click", "#popover-no", function () {
        $(this).parent().parent().find('#isTeam').popover('destroy');

    });
        

    $('.team #isTeam').each(function () {
        var bool = ($(this).parent().find("#IsExclude").val()) == "True";
        if (bool) {
            $(this).attr('disabled', 'disabled');
        }
    });
       


    $(".team-info #ReSend").click(function () {
        var id = $(this).parent().parent().find('#email').val();
        var data = { email: id };
        var textBox = $(this);
        $.ajax({
            type: "Put",
            url: baseUri + 'Team' + '/ReSend',
            dataType: 'json',
            data: data,
            statusCode: {
                200: function (data) {
                    location.reload();
                }
            }
        });



    });


});
