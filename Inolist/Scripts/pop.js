﻿

$(document).ready(function () {


    $("#ui-datepicker-div").remove();
    var nowTemp = new Date();
    $("#EndDate").datepicker({ minDate: new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate()), dateFormat: "dd.mm.yy" });
    $.validator.unobtrusive.parse("form");

    $('#submitAdd').click(function () {
        if ($("#formAdd").valid()) {
            $("#formAdd").submit();

        }
    });
    $('#submitEdit').click(function () {
        if ($("#formEdit").valid()) {
            $("#formEdit").submit();
        }
    });

    $('#delete').click(function () {
        var id = $(this).parent().parent().children('.modal-body').children("#formEdit").find('input[type="hidden"]').val();
        var data = { Id: id };
        $.ajax({
            type: "Delete",
            url: baseUri + 'Task/Delete/' + id,
            dataType: 'json',
            data: data,
            statusCode: {
                202: function () {
                    location.reload(true);
                }
            }
        });

    });
});
//@ sourceURL=sourcename