﻿using System;

using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MailLibrary
{
    public class Mail : MailMessage
    {
        private const String SendInviteOldTitle = "Приложение Inolist: Приглашение в команду";
        private const String SendInviteNewTitle = "Приглашение в Inolist";
        private const String SetTaskExecutorTitle = "У вас новая задача";
        private const String SetOffTaskExecutorTitle = "С вас сняли задачу";
        private const String TaskEditTitle = "Задача изменена";
        private const String TaskDoneTitle = "Задача выполнена";
        private const String TaskDeleteTitle = "Задача удалена";
        private const String TaskReOpenTitle = "Задача переоткрыта";
        private const String Host = "smtp.denakol.ru";
        private const String UserName = "postmaster@denakol.ru";
        private const String UserPassword = "hemativ4ra";

        public static void SendInvitesNewUser(String to, String from, String nameUser)
        {
            String body =
                String.Format(
                    @"Пользователь {0} приглашает вас в приложение Inolist <br> Пройдите по ссылке чтобы присоединится <a href=""http://denakol.ru/Account/GetVkAuth?email={1}""> перейти </a> ",
                    nameUser, to);
            var mail = new MailMessage(from, to, SendInviteNewTitle, body);
            SendMail(mail);
        }

        public static void SendInvitesOldUser(String to, String from, String nameUser, Int32 senderId)
        {
            String body =
                String.Format(
                    @"Пользователь {0} приглашает вас в команду <br> Пройдите по ссылке чтобы присоединится <a href=""http://denakol.ru/Team/confirm?email={1}&&senderId={2}""> перейти </a> ",
                    nameUser, to, senderId);
            var mail = new MailMessage(from, to, SendInviteOldTitle, body);
            SendMail(mail);
        }

        public static void SetTaskExecutor(String to, String from, String nameUser, String taskName, DateTime? endDate)
        {
            String body = String.Format("Пользователь {0} назначил на вас следующую задачу:{1} <br>", nameUser, taskName);
            String bodyDate = endDate.HasValue
                                  ? String.Format("Срок исполнения: {0} <br>", endDate.Value.ToShortDateString())
                                  : "";
            body = body + bodyDate + @"<a href=""http://denakol.ru/Task""> Перейти </a>";
            var mail = new MailMessage(from, to, SetTaskExecutorTitle, body);
            SendMail(mail);
        }

        public static void SetOffTaskExecutor(String to, String from, String nameUser, String taskName,
                                              DateTime? endDate)
        {
            String body =
                String.Format(
                    @"Пользователь {0} снял с вас следующую задачу:{1} <br>  <a href=""http://denakol.ru/Task""> Перейти </a>",
                    nameUser, taskName);
            var mail = new MailMessage(from, to, SetOffTaskExecutorTitle, body);
            SendMail(mail);
        }

        public static void TaskEdit(String to, String from, String nameUser, String taskName)
        {
            String body =
                String.Format(
                    @"Пользователь {0} изменил следующую задачу:{1} <br>  <a href=""http://denakol.ru/Task""> Перейти </a>",
                    nameUser, taskName);
            var mail = new MailMessage(from, to, TaskEditTitle, body);
            SendMail(mail);
        }


        public static void TaskDone(String to, String from, String nameUser, String taskName)
        {
            String body =
                String.Format(
                    @"Пользователь {0} выполнил следующую задачу:{1} <br>  <a href=""http://denakol.ru/Task""> Перейти </a>",
                    nameUser, taskName);
            var mail = new MailMessage(from, to, TaskDoneTitle, body);
            SendMail(mail);
        }

        public static void TaskDelete(String to, String from, String nameUser, String taskName)
        {
            String body =
                String.Format(
                    @"Пользователь {0} удалил следующую задачу:{1} <br>  <a href=""http://denakol.ru/Task""> Перейти </a>",
                    nameUser, taskName);
            var mail = new MailMessage(from, to, TaskDeleteTitle, body);
            SendMail(mail);
        }

        public static void TaskReOpen(String to, String from, String nameUser, String taskName)
        {
            var body =
                String.Format(
                    @"Пользователь {0} переоткрыл следующую задачу:{1} <br>  <a href=""http://denakol.ru/Task""> Перейти </a>",
                    nameUser, taskName);
            var mail = new MailMessage(from, to, TaskReOpenTitle, body);
            SendMail(mail);
        }


        private static void SendMail(MailMessage mail)
        {
            mail.IsBodyHtml = true;
            var smtp = new SmtpClient(Host)
                {
                    Credentials = new System.Net.NetworkCredential(UserName, UserPassword),
                    Port = 25
                };
            smtp.Send(mail);
        }
    }
}
