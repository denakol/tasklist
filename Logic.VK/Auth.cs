﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Web.Mvc;
using Logic.VK.Entity;
using System.Web.Script.Serialization;

namespace Logic.VK
{
    /// <summary>
    /// Класс для авторизации
    /// </summary>
    public class Auth : Controller
    {
        public ActionResult Authorization()
        {
            String clientID = System.Configuration.ConfigurationManager.AppSettings["ClientID"];
            String redirectUrl = System.Configuration.ConfigurationManager.AppSettings["RedirectUrl"];

            return Redirect(@"http://api.vk.com/oauth/authorize?client_id=" + clientID + "&redirect_uri=" + redirectUrl + "&scope=notify,friends,wall&response_type=code");
        }

        /// <summary>
        /// Получить ACCESS_TOKEN из Code
        /// </summary>
        public VKSecurity GetAccessToken(String code)
        {
            String clientID = System.Configuration.ConfigurationManager.AppSettings["ClientID"];
            String clientSecret = System.Configuration.ConfigurationManager.AppSettings["ClientSecret"];
            String redirectUrl = System.Configuration.ConfigurationManager.AppSettings["RedirectUrl"];

            try
            {
                String request = @"https://api.vkontakte.ru/oauth/access_token?client_id=" + clientID + "&client_secret=" + clientSecret + "&code=" + code + "&redirect_uri=" + redirectUrl;
                System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
                System.Net.WebResponse resp = reqGET.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                String data = sr.ReadToEnd();

                return SerilizerVKSecurityModel(data);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private VKSecurity SerilizerVKSecurityModel(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKSecurity>(data);
        }
    }
}
