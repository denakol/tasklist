﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

using Logic.VK.Entity;

namespace Logic.VK.Repository
{
    /// <summary>
    /// VK friend repo
    /// </summary>
    public class VKFriendRepo
    {
        /// <summary>
        /// Возвращает список друзей пользователя.
        /// </summary>
        /// <param name="uid">Идентификатор пользователя, для которого необходимо получить список друзей.</param>
        /// <param name="fields">Перечисленные через запятую поля анкет, необходимые для получения. Доступные значения: uid, first_name, last_name, nickname, screen_name, sex, bdate (birthdate), city, country, timezone, photo, photo_medium, photo_big, has_mobile, rate, contacts, education, online, counters.</param>
        /// <param name="count">Количество возвращаемых пользователей.</param>
        /// <param name="nameCase">Падеж для склонения имени и фамилии пользователя. Возможные значения: именительный – nom, родительный – gen, дательный – dat, винительный – acc, творительный – ins, предложный – abl.</param>
        /// <param name="accessToken">Строка безопасности</param>
        public VKUserList GetFriend(String uid, String fields, String count, String nameCase, String accessToken)
        {
            String request = String.Format("https://api.vkontakte.ru/method/friends.get?uid={0}&fields={1}&name_case={2}&access_token={3}", uid, fields, nameCase, accessToken);
            System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
            System.Net.WebResponse resp = reqGET.GetResponse();
            System.IO.Stream stream = resp.GetResponseStream();
            System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            String data = sr.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            VKFriendList vkFriendList = serializer.Deserialize<VKFriendList>(data);

            if (vkFriendList != null && vkFriendList.response != null && vkFriendList.response.Count != 0)
            {
                String uids = vkFriendList.response[0].ToString();

                VKUserList vkUserList = new VKUserList();
                for (Int32 i = 1; i < vkFriendList.response.Count; i++)
                {
                    uids += "," + vkFriendList.response[i].ToString();
                }

                return (new VKUserRepo()).GetUsers(uids, fields, count, nameCase, accessToken);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Возвращает список общих друзей двух пользователей.
        /// </summary>
        /// <param name="targetUid">Идентификатор пользователя, с которым необходимо искать общих друзей.</param>
        /// <param name="sourceUid">идентификатор пользователя, чьи друзья пересекаются с друзьями пользователя с идентификатором target_uid.</param>
        /// <param name="fields">Перечисленные через запятую поля анкет, необходимые для получения. Доступные значения: uid, first_name, last_name, nickname, screen_name, sex, bdate (birthdate), city, country, timezone, photo, photo_medium, photo_big, has_mobile, rate, contacts, education, online, counters.</param>
        /// <param name="count">Количество возвращаемых пользователей.</param>        
        /// <param name="nameCase">Падеж для склонения имени и фамилии пользователя. Возможные значения: именительный – nom, родительный – gen, дательный – dat, винительный – acc, творительный – ins, предложный – abl.</param>        
        /// <param name="accessToken">Строка безопасности</param>
        public VKUserList GetMutualFriend(String targetUid, String sourceUid, String fields, String count, String nameCase, String accessToken)
        {
            String request = String.Format("https://api.vkontakte.ru/method/friends.getMutual?target_uid={0}&source_uid={1}&access_token={2}", targetUid, sourceUid,accessToken);
            System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
            System.Net.WebResponse resp = reqGET.GetResponse();
            System.IO.Stream stream = resp.GetResponseStream();
            System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            String data = sr.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            VKFriendList vkFriendList = serializer.Deserialize<VKFriendList>(data);

            if (vkFriendList != null && vkFriendList.response != null && vkFriendList.response.Count != 0)
            {
                String uids = vkFriendList.response[0].ToString();

                VKUserList vkUserList = new VKUserList();
                for (Int32 i = 1; i < vkFriendList.response.Count; i++)
                {
                    uids += "," + vkFriendList.response[i].ToString();
                }

                return (new VKUserRepo()).GetUsers(uids, fields, count, nameCase, accessToken);
            }
            else
            {
                return null;
            }
        }

        private VKUserList SerilizerVKUserList(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKUserList>(data);
        }
    }
}
