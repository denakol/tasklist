﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;

using Logic.VK.Entity;

namespace Logic.VK.Repository
{
    /// <summary>
    /// VK user repo
    /// </summary>
    public class VKUserRepo
    {
        /// <summary>
        /// Возвращает расширенную информацию о пользователях. 
        /// </summary>
        /// <param name="uids">Перечисленные через запятую ID пользователей.</param>
        /// <param name="fields">Перечисленные через запятую поля анкет, необходимые для получения. Доступные значения: uid, first_name, last_name, nickname, screen_name, sex, bdate (birthdate), city, country, timezone, photo, photo_medium, photo_big, has_mobile, rate, contacts, education, online, counters.</param>
        /// <param name="count">Количество возвращаемых пользователей.</param>
        /// <param name="nameCase">Падеж для склонения имени и фамилии пользователя. Возможные значения: именительный – nom, родительный – gen, дательный – dat, винительный – acc, творительный – ins, предложный – abl.</param>
        /// <param name="accessToken">Строка безопасности</param>
        public VKUserList GetUsers(String uids, String fields, String count, String nameCase, String accessToken)
        {
            try
            {
                String request = String.Format("https://api.vkontakte.ru/method/users.get?uids={0}&fields={1}&name_case={2}&access_token={3}", uids, fields, nameCase, accessToken);
                System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
                System.Net.WebResponse resp = reqGET.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                String data = sr.ReadToEnd();

                return SerilizerVKUserList(data);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Возвращает список пользователей в соответствии с заданным критерием поиска. 
        /// </summary>
        /// <param name="query">Строка поискового запроса.</param>
        /// <param name="fields">Перечисленные через запятую поля анкет, необходимые для получения. Доступные значения: uid, first_name, last_name, nickname, screen_name, sex, bdate (birthdate), city, country, timezone, photo, photo_medium, photo_big, has_mobile, rate, contacts, education, online, counters.</param>
        /// <param name="count">Количество возвращаемых пользователей.</param>
        /// <param name="accessToken">Строка безопасности</param>
        public VKUserList SearchUsers(String query, String fields, String count, String accessToken)
        {
            try
            {
                String request = String.Format("https://api.vkontakte.ru/method/users.search?q={0}&fields={1}&count={2}&access_token={3}", query, fields, count, accessToken);
                System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
                System.Net.WebResponse resp = reqGET.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                String data = sr.ReadToEnd();

                VKList vkList = SerilizerVKList(data);

                if (vkList != null && vkList.response != null && vkList.response.Count != 0)
                {
                    vkList.response.RemoveAt(0);

                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    data = serializer.Serialize(vkList);

                    return serializer.Deserialize<VKUserList>(data);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Получает настройки текущего пользователя в данном приложении. Справочник кодов ответов/ошибок функции http://vk.com/pages?oid=-1&p=getUserSettings
        /// </summary>
        /// <param name="uid">ID текущего пользователя.</param>
        /// <param name="accessToken">Строка безопасности</param>
        /// <returns></returns>
        public VKUserSetting SettingsUser(String uid, String accessToken)
        {
            try
            {
                String request = String.Format("https://api.vkontakte.ru/method/getUserSettings?uid={0}&access_token={1}", uid, accessToken);
                System.Net.WebRequest reqGET = System.Net.WebRequest.Create(request);
                System.Net.WebResponse resp = reqGET.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                String data = sr.ReadToEnd();

                return SerilizerVKSettingUserModel(data);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private VKUser SerilizerVKUser(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKUser>(data);
        }

        private VKUserList SerilizerVKUserList(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKUserList>(data);
        }

        private VKUserSetting SerilizerVKSettingUserModel(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKUserSetting>(data);
        }

        private VKList SerilizerVKList(String data)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<VKList>(data);
        }
    }
}
