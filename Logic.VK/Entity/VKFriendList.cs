﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// VKontakte friend list
    /// </summary>
    public class VKFriendList
    {
        /// <summary>
        /// Response
        /// </summary>
        public List<Int32> response { get; set; }
    }
}
