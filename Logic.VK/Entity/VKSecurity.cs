﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// VK security
    /// </summary>
    public class VKSecurity
    {
        /// <summary>
        /// UID
        /// </summary>
        public String user_id { get; set; }

        /// <summary>
        /// Access token
        /// </summary>
        public String access_token { get; set; }

        /// <summary>
        /// Expires in
        /// </summary>
        public String expires_in { get; set; }
    }
}
