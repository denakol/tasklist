﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// VKontakte user setting
    /// </summary>
    public class VKUserSetting
    {
        /// <summary>
        /// Response
        /// </summary>
        public String response { get; set; }
    }
}
