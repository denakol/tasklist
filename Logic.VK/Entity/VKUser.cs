﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// User VKontakte
    /// </summary>
    public class VKUser
    {
        #region fields

        /// <summary>
        /// VK ID (without 'id')
        /// </summary>
        public String uid { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        public String first_name { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public String last_name { get; set; }

        /// <summary>
        /// Nick name
        /// </summary>
        public String nickname { get; set; }

        /// <summary>
        /// VK ID (with 'id')
        /// </summary>
        public String screen_name { get; set; }

        /// <summary>
        /// Sex
        /// </summary>
        public String sex { get; set; }

        /// <summary>
        /// Birthdate
        /// </summary>
        public String bdate { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public String city { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public String country { get; set; }

        /// <summary>
        /// Timezone
        /// </summary>
        public String timezone { get; set; }

        /// <summary>
        /// Custom photo URL
        /// </summary>
        public String photo { get; set; }

        /// <summary>
        /// Medium photo URL
        /// </summary>
        public String photo_medium { get; set; }

        /// <summary>
        /// Big photo URL
        /// </summary>
        public String photo_big { get; set; }

        /// <summary>
        /// Has mobile
        /// </summary>
        public String has_mobile { get; set; }

        /// <summary>
        /// Rate
        /// </summary>
        public String rate { get; set; }

        /// <summary>
        /// Mobile phone
        /// </summary>
        public String mobile_phone { get; set; }

        /// <summary>
        /// Home phone
        /// </summary>
        public String home_phone { get; set; }

        /// <summary>
        /// University
        /// </summary>
        public String university { get; set; }

        /// <summary>
        /// University name
        /// </summary>
        public String university_name { get; set; }

        /// <summary>
        /// Faculty
        /// </summary>
        public String faculty { get; set; }

        /// <summary>
        /// Faculty name
        /// </summary>
        public String faculty_name { get; set; }

        /// <summary>
        /// Graduation
        /// </summary>
        public String graduation { get; set; }

        /// <summary>
        /// Online
        /// </summary>
        public String online { get; set; }

        #endregion fields
    }
}
