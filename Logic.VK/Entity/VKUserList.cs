﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// VKontakte user list
    /// </summary>
    public class VKUserList
    {
        /// <summary>
        /// Response
        /// </summary>
        public List<VKUser> response { get; set; }
    }
}
