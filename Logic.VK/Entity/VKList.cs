﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic.VK.Entity
{
    /// <summary>
    /// VKList
    /// </summary>
    public class VKList
    {
        /// <summary>
        /// Response
        /// </summary>
        public List<Object> response { get; set; }
    }
}
